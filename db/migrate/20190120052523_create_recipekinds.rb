class CreateRecipekinds < ActiveRecord::Migration[5.2]
  def change
    create_table :recipekinds do |t|
      t.integer :recipe_id, unique: true
      t.string :kind

      t.timestamps
    end
  end
end
