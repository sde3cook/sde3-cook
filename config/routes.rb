Rails.application.routes.draw do

  get 'recipes/index'
  get 'recipes/new'
  get 'recipes/create'
  get 'recipes/edit'
  get 'recipes/update'
  get 'recipes/delete'

  get 'recipes/new_recipe'
  get 'recipes/new_recipe2'
  get 'recipes/rank_day', to: 'recipes#rank_day'
  get 'recipes/rank_week', to: 'recipes#rank_week'
  get 'recipes/menu', to: 'recipes#menu'
  get 'recipes/prof'
  get 'recipe_search', to: 'recipes#recipe_search'


  devise_for :users
  resources :recipes do
    get "favorites/toggleFav"
  end

  root to:'recipes#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
