class CreateZairyos < ActiveRecord::Migration[5.2]
  def change
    create_table :zairyos do |t|
      t.integer :recipe_id
      t.string :name
      t.string :kosu

      t.timestamps
    end
  end
end
