# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
favorite_icon = ->
  $("#favorite-link").click ->
    $(this).children('i').toggleClass('fa-heart-o')

$(document).on('page:change', favorite_icon)
