class RecipesController < ApplicationController
  before_action :set_recipe, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:edit, :update, :destroy]
  # GET /recipes
  # GET /recipes.json
  def index
    @q = Recipe.ransack(params[:q])
    @recipes = @q.result.includes(:user).paginate(page: params[:page], per_page: 3).order(created_at: :desc)
    @recipes_new = @q.result.includes(:user).paginate(page: params[:page], per_page: 4).order(created_at: :desc)
    @day_ranking = Favorite.includes(:recipe).where(created_at: Date.today.all_day).group(:recipe_id).order("count(*) desc").limit(3)
    threedays_menu
  end


  def new_recipe
    @q = Recipe.ransack(params[:q])
    @recipes = @q.result.includes(:user).paginate(page: params[:page], per_page: 9).order(created_at: :desc)
    @page= @recipes.current_page
  end

  # GET /recipes/1
  # GET /recipes/1.json
  def show
    @q = Recipe.ransack(params[:q])
  end

  # GET /recipes/new
  def new
    @q = Recipe.ransack(params[:q])
    if user_signed_in?
      @recipe = Recipe.new
      @recipe.recipekinds.build
    else
      redirect_to new_user_registration_path, notice:"会員のみレシピ投稿ができます！"
    end
  end

  # GET /recipes/1/edit
  def edit
    @q = Recipe.ransack(params[:q])
  end

  # POST /recipes
  # POST /recipes.json
  def create
    @q = Recipe.ransack(params[:q])
    @recipe = Recipe.new(recipe_params)

    respond_to do |format|
      if @recipe.save
        format.html { redirect_to @recipe, notice: 'レシピを投稿しました！' }
        format.json { render :show, status: :created, location: @recipe }
      else
        format.html { render :new }
        format.json { render json: @recipe.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /recipes/1
  # PATCH/PUT /recipes/1.json
  def update
    @q = Recipe.ransack(params[:q])
    respond_to do |format|
      if @recipe.update(recipe_params)
        format.html { redirect_to @recipe, notice: 'レシピの編集が完了しました！' }
        format.json { render :show, status: :ok, location: @recipe }
      else
        format.html { render :edit }
        format.json { render json: @recipe.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /recipes/1
  # DELETE /recipes/1.json
  def destroy
    @recipe.destroy
    respond_to do |format|
      format.html { redirect_to recipes_url, notice: 'レシピを削除しました！' }
      format.json { head :no_content }
    end
  end

  def recipe_search
    @q = Recipe.search(params[:q])
    @recipes = @q.result.includes(:user).paginate(page: params[:page], per_page: 9).order(created_at: :desc)
    @page = @recipes.current_page
    @recipes_all = Recipe.includes(:user).paginate(page: params[:page], per_page: 9).order(created_at: :desc)
  end

  def rank_day
    @q = Recipe.ransack(params[:q])
    ranks
  end

  def rank_week
    @q = Recipe.ransack(params[:q])
    ranks
  end

  def menu
    @q = Recipe.ransack(params[:q])
    threedays_menu
  end

  def prof
    @q = Recipe.ransack(params[:q])
  end

  private

    def threedays_menu
      @main = Recipekind.includes(:recipe).where(kind: 'ご飯').order("RANDOM()").limit(3)
      @side = Recipekind.includes(:recipe).where(kind: 'おかず').order("RANDOM()").limit(3)
      @soup = Recipekind.includes(:recipe).where(kind: '汁物').order("RANDOM()").limit(3)
    end

    def ranks
      @week_ranking = Favorite.includes(:recipe).where(created_at: Date.today.all_week).group(:recipe_id).order("count(*) desc").limit(12)
      @day_ranking = Favorite.includes(:recipe).where(created_at: Date.today.all_day).group(:recipe_id).order("count(*) desc").limit(12)
    end

    def search_params
      params.require(:q).permit(:title_cont)
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_recipe
      @recipe = Recipe.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def recipe_params
      params.require(:recipe).permit(:user_id, :title, :img, :img_cache, :remove_img, :cook_min,
         zairyos_attributes: [:id, :name, :kosu, :_destroy],
         recipekinds_attributes: [:id, :kind],
         recipeprocesses_attributes: [:id, :process_txt, :process_img, :_destroy, :remove_process_img, ])
    end
end
